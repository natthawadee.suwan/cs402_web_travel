from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from hotel.models import Hotels, Review, Room, Proposal, Photo, Reservation
from django.views import generic
from django.contrib.auth.decorators import login_required
from question.decorators import active_required
from django import forms
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.utils.decorators import method_decorator
# from django.core.urlresolvers import reverse,reverse_lazy
# from django.core.urlresolvers import reverse
from django.urls import reverse, reverse_lazy
from django.views import View
import random
from django.db.models import Q, Sum


# @login_required
# @active_required
def hotelindex(request):
    hotels_list = Hotels.objects.all()
    for hotel in hotels_list:
        hotel.thumbnail = Photo.objects.filter(hotel=hotel).first()
    context = {'hotels': hotels_list}
    return render(request, 'hotel/hotel_view.html', context)


def regcomplete(request):
    link = reverse('hotel:hotel_view')
    return HttpResponseRedirect(link)


def hoteldetails(request, pk):
    theuser = request.user
    #thehotel = Category.objects.filter(id = pk)[0]

    thehotel = Hotels.objects.get(id=pk)
    RecentReservation = Reservation.objects.filter(hotel=thehotel).filter(user=theuser)

    if RecentReservation:
        allowReview = True
    else:
        allowReview = False

    reviews = Review.objects.filter(hotel=thehotel)
    rooms = Room.objects.filter(hotel=thehotel)
    city = thehotel.city
    NearbyHotels = Hotels.objects.filter(city=city).exclude(id=thehotel.id)
    Nearbyid = []

    for Hotel in NearbyHotels:
        Nearbyid.append(Hotel.id)
    if not Nearbyid:
        Recommendation = None
    else:
        randomid = random.choice(Nearbyid)
        Recommendation = Hotels.objects.get(id=randomid)

    photos = Photo.objects.filter(hotel=thehotel)
    FirstDate = request.session['checkin']
    SecDate = request.session['checkout']
    current_user = request.user

    # less than or equal 
    for room in rooms:
        RoomsBooked = Reservation.objects.filter(room=room).filter(check_in__lte=SecDate,
                                                                   check_out__gte=FirstDate)
        count = RoomsBooked.count()
        count = int(count)
        Roomsavailable = room.total_rooms
        Roomsavailable = int(Roomsavailable)

        Roomsleft = Roomsavailable - count
        room.spaceleft = Roomsleft

    # work out rating
    NumReviews = Reservation.objects.filter(hotel=thehotel).count()
    totalrating = Review.objects.filter(hotel=thehotel).aggregate(sum=Sum('rating'))['sum']

    if totalrating:
        Rating = round(totalrating/NumReviews)
    else:
        Rating = None

    if Rating:
        if Rating >= 80:
            starpath = '5star.png'
        elif Rating >= 60:
            starpath = '4star.png'
        elif Rating >= 40:
            starpath = '3star.png'
        elif Rating >= 20:
            starpath = '2star.png'
        elif Rating < 20:
            starpath = '1star.png'

    if Rating == None:
        starpath = 'NR.png'

    room = rooms[0]
    room_images = []
    if room.photo_1 != "":
        room_images.append(room.photo_1)

    if room.photo_2 != "":
        room_images.append(room.photo_2)

    if room.photo_3 != "":
        room_images.append(room.photo_3)

    if room.photo_4 != "":
        room_images.append(room.photo_4)

    if room.photo_5 != "":
        room_images.append(room.photo_5)

    context = {'hotels': thehotel, 'reviews': reviews, 'user': current_user, 'rooms': rooms, 'Photos': photos,
               'Recommended': Recommendation, 'Rating': Rating, 'starpath': starpath, 'allowReview': allowReview,
               'room_images':room_images }
    return render(request, 'hotel/hotel_details.html', context)


# ================================ Review ================================
class reviewCreateView(CreateView):
    model = Review
    fields = ['comment', 'rating']
    #success_url = '/hotels/'

    def get_success_url(self):
        hotelid = self.kwargs['id']
        return reverse('hotel:hotel_detail', args=[hotelid])

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.instance.hotel_id = self.kwargs['id']
        return super(reviewCreateView, self).form_valid(form)


class reviewUpdateView(UpdateView):
    model = Review
    fields = ['comment', 'rating']
    #success_url = '/hotels/'

    def get_success_url(self):
        reviewid = self.kwargs['pk']
        review = Review.objects.get(id=reviewid)
        hotel = review.hotel
        hotelid = hotel.id
        url = reverse('hotel:hotel_detail', args=[hotelid])
        return url

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(reviewUpdateView, self).form_valid(form)


class reviewDeleteView(DeleteView):
    model = Review

    def get_success_url(self):
        reviewid = self.kwargs['pk']
        review = Review.objects.get(id=reviewid)
        hotel = review.hotel
        hotelid = hotel.id
        return reverse('hotel:hotel_detail', args=[hotelid])

# ================================ End Review ================================


class partnerCreateView(CreateView):
    model = Proposal
    fields = ['CompanyName', 'CompanyEmail', 'HQAddress', 'Vision']
    #success_url = '/hotels/'

    def get_success_url(self):
        url = reverse('hotel:hotel_view')
        return url

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(partnerCreateView, self).form_valid(form)


class hotelSearch(View):
    def get(self, request):
        return render(request, 'hotel/hotel_search.html')

    def post(self, request):
        Searchterm = request.POST.get("searchterm").title()
        NumTravelers = request.POST.get("numtravelers")

        if not Searchterm and not NumTravelers:
            hotels_list = Hotels.objects.all()
        elif Searchterm and not NumTravelers:
            hotels_list = Hotels.objects.filter(Q(city__contains=Searchterm) | Q(Address__contains=Searchterm))
            # hotels_list = Hotels.objects.filter(Q(city__contains=Searchterm) | Q(Country__contains=Searchterm)
            #                                     | Q(Address__contains=Searchterm))
        elif NumTravelers and not Searchterm:
            hotels_list = Hotels.objects.filter(
                room__capacity__gte=NumTravelers)
        elif Searchterm and NumTravelers:
            hotels_list = Hotels.objects.filter(Q(city__contains=Searchterm) | Q(Address__contains=Searchterm)).filter(room__capacity__gte=NumTravelers)
            # hotels_list = Hotels.objects.filter(Q(city__contains=Searchterm) | Q(Country__contains=Searchterm)
            #                                     | Q(Address__contains=Searchterm)).filter(room__capacity__gte=NumTravelers)
        Range = request.POST.get("daterange")
        Rangesplit = Range.split(' to ')
        CheckIn = Rangesplit[0]
        CheckOut = Rangesplit[1]
        request.session['checkin'] = CheckIn
        request.session['checkout'] = CheckOut
        for hotel in hotels_list:
            hotel.thumbnail = Photo.objects.filter(hotel=hotel).first()
        context = {'hotels': hotels_list}
        return render(request, 'hotel/hotel_view.html', context)
