import datetime
from django import forms
from django.shortcuts import render, redirect
# from django.core.urlresolvers import reverse,reverse_lazy
from django.urls import reverse, reverse_lazy
from django.core.signing import Signer
from django.http import HttpResponse,HttpResponseRedirect
from django.db.models import Q
from hotel.models import Hotels, Room, Proposal, Reservation, Photo
from account.models import Account
from django.utils.translation import ugettext_lazy as _
# from Reservations.models import Reservation

from django.views import View
from django.template.loader import get_template
from hotel.utils import render_to_pdf

## Generates a PDF using the render help function and outputs it as invoice.html
class GeneratePDF(View):
    def get(self,request, *args, **kwargs):
        booking = Reservation.objects.get(id= self.kwargs['id'])
        template = get_template('invoice.html')
        context = {"booking":booking}
        html = template.render(context)
        pdf = render_to_pdf('invoice.html', context)
        return HttpResponse(pdf,content_type='application/pdf')



## Works out how long the user is staying in a hotel for also working out the total cost.
def bookRoom(request,hotelid,roomid):

    FirstDate = request.session['checkin']
    SecDate =  request.session['checkout']

    Checkin = datetime.datetime.strptime(FirstDate, "%Y-%m-%d").date()
    Checkout = datetime.datetime.strptime(SecDate, "%Y-%m-%d").date()
    timedeltaSum = Checkout - Checkin

    StayDuration = timedeltaSum.days

    Hotel = Hotels.objects.get(id = hotelid)
    theRoom = Room.objects.get(id = roomid)

    price = theRoom.price
    TotalCost = StayDuration * price


    context = {'checkin': Checkin, 'checkout':Checkout,'stayduration':StayDuration,'hotel':Hotel,'room':theRoom,'price':price,
    'totalcost':TotalCost}
    return render(request, 'reservation/booking.html', context)


# Stores the confirmed booking  into the database
def booking_create(request,hotelid,roomid,checkin,checkout,totalcost, *args, **kwargs):
    if request.method == 'POST':
        Firstname = request.POST.get('firstname')
        Lastname = request.POST.get('lastname')

        user        = request.user
        hotel       = Hotels.objects.get(id = hotelid)
        room        = Room.objects.get(id = roomid)
        cost        = totalcost
        newReservation                  = Reservation()
        newReservation.hotel            = hotel
        newReservation.room             = room
        newReservation.user             = user
        newReservation.guest_firstName   = Firstname
        newReservation.guest_lastName    = Lastname
        newReservation.check_in          = checkin
        newReservation.check_out         = checkout
        newReservation.total_price       = cost
        newReservation.save()
        #Deletes the session variables.
        del request.session['checkin']
        del request.session['checkout']
        
        # context = {}
        # context['success_message'] = "Updated"

        # return reverse('hotel:hotel_view')
        # return HttpResponseRedirect(reverse('hotel:hotel_view'))
        # return render(request, "trip/hotel/hotel_view.html", context)
        # return redirect('home')
        return redirect('reservation:bookings_view')
        # message = _("form for customer xyz was successfully updated...")
        # request.user.message_set.create(message = message)
        # return redirect('reservation:bookings_view')
        # return redirect(HttpResponseRedirect('reservation:bookings_view', kwargs={ context }))
    else:
        # return reverse('hotel:hotel_view')
        return redirect('hotel:hotel_view')


#Shows the user thier previous bookings.
def mybookings(request, *args, **kwargs):
    bookings = Reservation.objects.filter(user = request.user)
    context = {'bookings':bookings}
    return render(request, 'reservation/mybookings.html', context)


# Allows a user to cancel their previous bookings , deleting a booking onclick.
def cancelbooking(request, id):
    booking = Reservation.objects.filter(id = id)
    booking.delete()
    # currentuser = request.is_active
    # Role = Account.objects.get(user = request.is_active)
    link = reverse('reservation:bookings_view')
    return HttpResponseRedirect(link)
