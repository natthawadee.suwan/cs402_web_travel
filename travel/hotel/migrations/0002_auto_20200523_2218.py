# Generated by Django 3.0.5 on 2020-05-23 15:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hotel', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='hotels',
            old_name='Partner',
            new_name='partner',
        ),
        migrations.RemoveField(
            model_name='hotels',
            name='Address',
        ),
        migrations.RemoveField(
            model_name='hotels',
            name='City',
        ),
        migrations.RemoveField(
            model_name='hotels',
            name='Country',
        ),
        migrations.RemoveField(
            model_name='hotels',
            name='Description',
        ),
        migrations.RemoveField(
            model_name='hotels',
            name='ImagePath',
        ),
        migrations.RemoveField(
            model_name='hotels',
            name='TelephoneNumber',
        ),
        migrations.AddField(
            model_name='hotels',
            name='address',
            field=models.CharField(max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='hotels',
            name='city',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='hotels',
            name='en_brief_description',
            field=models.TextField(max_length=500, null=True),
        ),
        migrations.AddField(
            model_name='hotels',
            name='image_path',
            field=models.CharField(max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='hotels',
            name='telephone_number',
            field=models.CharField(max_length=12, null=True),
        ),
        migrations.AddField(
            model_name='hotels',
            name='th_brief_description',
            field=models.TextField(max_length=500, null=True),
        ),
        migrations.AddField(
            model_name='hotels',
            name='time_check_in',
            field=models.TimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='hotels',
            name='time_check_out',
            field=models.TimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='hotels',
            name='zipcode',
            field=models.CharField(max_length=10, null=True),
        ),
    ]
