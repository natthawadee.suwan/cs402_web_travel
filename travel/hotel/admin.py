from django.contrib import admin
from hotel.models import Hotels, Review, Proposal, Photo, Reservation, Room

# Registers the Hotels and review models on the Django Admin page

admin.site.register(Photo)
admin.site.register(Review)
admin.site.register(Proposal)
admin.site.register(Reservation)

# Photo
# class PhotoInline(admin.TabularInline):
#     model = Photo

# class PhotoAdmin(admin.ModelAdmin):
#     inlines = [PhotoInline]

# admin.site.register(Hotels, PhotoAdmin) 

# Room
class RoomInline(admin.TabularInline):
    model = Room

class RoomAdmin(admin.ModelAdmin):
    inlines = [RoomInline]

admin.site.register(Hotels, RoomAdmin) 