# import string
# import secrets

# # Generate a 10-characters alphanumeric password
# # ascii_letters — contains both the lower case and upper case from A-Z
# alphabet = string.ascii_letters + string.digits
# password = ''.join(secrets.choice(alphabet) for i in range(10))
# # print(password)


# # Generate a 10-characters hexadecimal password with punctuation
# alphabet = string.hexdigits + string.punctuation
# password = ''.join(secrets.choice(alphabet) for i in range(10))
# # print(password)


# # Generate a 10-characters password with at least one lowercase, one uppercase, and one digit
# # islower — Determine if the character is lowercase
# # isupper — Determine if the character is uppercase
# # isdigit — Determine if the character is a digit
# alphabet = string.ascii_letters + string.digits
# while True:
#     password = ''.join(secrets.choice(alphabet) for i in range(10))
#     if (any(c.islower() for c in password) and any(c.isupper() for c in password) and any(c.isdigit() for c in password)):
#         break
# # print(password)



# # Generate a 10-characters password with at least two uppercase and two digits
# alphabet = string.ascii_letters + string.digits
# while True:
#     password = ''.join(secrets.choice(alphabet) for i in range(10))
#     if (sum(c.isupper() for c in password) >= 2 and sum(c.isdigit() for c in password) >= 2):
#         break
# # print(password)



# # Generate a four-word password that is unique
# animal = ['horse', 'elephant', 'monkey', 'donkey', 'goat', 'chicken', 'duck', 'mouse']
# fruit = ['apple', 'banana', 'peach', 'orange', 'papaya', 'watermelon', 'durian']
# electronic = ['computer', 'laptop', 'smartphone', 'battery', 'charger', 'cable']
# vegetable = ['lettuce', 'spinach', 'celery', 'cabbage', 'turnip', 'cucumber', 'eggplant']
# word_list = animal + fruit + electronic + vegetable
# password = set()
# while True:
#     password.add(secrets.choice(word_list))
#     if(len(password) >= 4):
#         break
# # print(' '.join(password))


# # Generate a temporary URL with security tokens for a password reset
# url = 'https://mywebsite/reset?key=' + secrets.token_urlsafe()      #secrets.token_urlsafe(32)
# print(url)
