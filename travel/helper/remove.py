

def person_create(request):
    location_start = load_location_start_end()

    styles = Style.objects.all()
    locations = Location.objects.all()
    categories = CategorySub.objects.all()

    data = {
        'styles': styles,
        'locations': locations,
        'provinces': location_start,
        'categories': categories,
    }

    template_name = 'trip/trip_person/person_create.html'

    return render(request, template_name, data)

def person_save(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        budget = request.POST.get('budget')
        time_start = request.POST.get('time')
        time_hotel = request.POST.get('time_hotel')
        child = request.POST.get('child')
        teens = request.POST.get('teens')
        elderly = request.POST.get('elderly')
        province = request.POST.get('province')
        seasonPK = request.POST.get('seasons')
        stylePK = request.POST.get('styles')
        landmarkPK = request.POST.get('landmarks')
        categories = request.POST.getlist('category')
        locations = request.POST.getlist('location')
        time_count = request.POST.getlist('time_count')
        description_text = request.POST.get('description')

        landmark_name = Landmark.objects.filter(pk=landmarkPK)
        landmark_name = landmark_name[0].name
        style_name = Style.objects.filter(pk=stylePK)
        style_name = style_name[0].name
        season_name = Season.objects.filter(pk=seasonPK)
        season_name = season_name[0]

        category_list = get_list_id_category_return_list_name_category(categories)


        location_time_list = []
        count = 0
        for item in locations:
            location_line = []
            location_line.append(item)
            location_line.append(time_count[count])
            location_time_list.append(location_line)
            count += 1

        # print(location_time_list)

        # save location
        location_list = []
        location_list.append(province)
        for location in locations:
            location_list.append(location)
        # print("location_list: {}".format(location_list))

        # input_location
        data = create_data(location_list)
        # print("data: {}".format(data))

        plan_list = run_data_model(data, location_list)
        row_list = create(location_list)
        # print(plan_list)
        # print("row_list: {}".format(row_list))

        duration_list = row_list[1]
        distance_list = row_list[0]
        # print("duration_list: {}".format(duration_list))
        # print("distance_list: {}".format(distance_list))
        # print("location_list: {}".format(location_list))

        trip_description = get_data_return_json(location_list, duration_list, distance_list, time_start, location_time_list, time_hotel)

        # Generate a temporary URL with security tokens for a password reset
        # url = 'https://mywebsite/reset?key=' + secrets.token_urlsafe()      #secrets.token_urlsafe(32)
        token = secrets.token_urlsafe()

        trip = Trip(token = token ,name = name, budget = budget, time_start = time_start, season = season_name, type_generate = "User",
                    child = child, teen = teens, elderly = elderly, location_start = province, 
                    trip_description_json = trip_description, trip_description_text = description_text)
        trip.save()

        for item in categories:
            # trip.tags.add(get_category_id_return_category_name(item))
            trip.category_sub.add(item)
        trip.style.add(stylePK)
        trip.landmark.add(landmarkPK)
        trip.tags.add(season_name.name, landmark_name, style_name)

        template = loader.get_template("trip/trip_person/person_create.html")
        return HttpResponse(template.render())
    else:
        template = loader.get_template("trip/trip_404.html")
        return HttpResponse(template.render())
        # return HttpResponse('<html><body>Data Error</body></html>')


def load_locations(request):
    category_id = request.GET.get('category')
    # print('===========category_id===========')
    # print(category_id)
    locations = Location.objects.filter(category_sub_id=category_id).order_by('name')
    # print('===========locations===========')
    # print(locations)
    return render(request, 'trip/trip_person/snippets/location_dropdown_list_options.html', {'locations': locations})
