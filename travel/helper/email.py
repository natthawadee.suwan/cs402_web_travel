from django.core.mail import send_mail, EmailMessage
from django.template.loader import render_to_string
import threading

# def send_html_email(user_email, subject, template_name, context):
#     to_list = [user_email]
#     msg_html = render_to_string(template_name, context)
#     msg = EmailMessage(subject, msg_html, to=to_list, from_email='noreply@tuvolunteer.tu.ac.th')
#     msg.content_subtype = "html"  # Main content is now text/html
#     msg.send()

class EmailThread(threading.Thread):
    def __init__(self, user_email, subject, template_name, context):
        self.subject = subject
        self.recipient_list = [user_email]
        self.html_content = render_to_string(template_name, context)
        threading.Thread.__init__(self)

    def run (self):
        msg = EmailMessage(self.subject, self.html_content, to=self.recipient_list, from_email='welcome.prasong@gmail.com')
        msg.content_subtype = "html"  # Main content is now text/html
        msg.send()

def send_html_email(user_email, subject, template_name, context):
    EmailThread(user_email, subject, template_name, context).start()

    