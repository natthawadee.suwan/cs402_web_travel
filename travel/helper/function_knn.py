# coding=utf-8

import pandas as pd
import numpy as np

# explicitly require this experimental feature
from sklearn.experimental import enable_iterative_imputer  # noqa
# now you can import normally from sklearn.impute
from sklearn.impute import IterativeImputer
from sklearn.ensemble import ExtraTreesRegressor
from sklearn.preprocessing import OrdinalEncoder

# #instantiate both packages to use
# encoder = OrdinalEncoder()
# imputer = IterativeImputer(ExtraTreesRegressor())

#use function : create a for loop to iterate through each column in the data
def encode(data):
    '''function to encode non-null data and replace it in the original data'''
    encoder = OrdinalEncoder()
    #retains only non-null values
    nonulls = np.array(data.dropna())
    #reshapes the data for encoding
    impute_reshape = nonulls.reshape(-1,1)
    #encode date
    impute_ordinal = encoder.fit_transform(impute_reshape)
    #Assign back encoded values to non-null values
    data.loc[data.notnull()] = np.squeeze(impute_ordinal)
    return data


def encode_data(data):
    imputer = IterativeImputer(ExtraTreesRegressor())
    # impute data and convert 
    encode_data = pd.DataFrame(np.round(imputer.fit_transform(data)),columns = data.columns)
    # encode_data.info()
    # print(encode_data.head())
    return encode_data



# https://github.com/chrisdev/django-pandas/