# coding=utf-8

import pandas as pd
import numpy as np

# explicitly require this experimental feature
from sklearn.experimental import enable_iterative_imputer  # noqa

#from sklearn.neighbors import KNeighborsRegressor
#import the KNeighborsClassifier class from sklearn

# splitting the data into training and test sets (80:20)
from sklearn.model_selection import train_test_split

#import the KNeighborsClassifier class from sklearn
from sklearn.neighbors import KNeighborsClassifier

#import metrics model to check the accuracy 
from sklearn import metrics

from function_knn import encode, encode_data

#Create bunch object containing iris dataset and its attributes.
result = pd.read_csv("../static/knn/result.csv")

# print("First five rows")
# print(result.head())


# create a list of categorical columns to iterate over
cat_cols = ['คุณมาจากภูมิลําเนาใด','รายได้ต่อเดือน','อาชีพ','อายุ','เพศ','โครงสร้างครอบครัว','ความถี่ของการเดินทางท่องเที่ยวของท่านมีลักษณะอยู่ในข้อใด?','จุดมุ่งหมายในการมายังจังหวัดกาญจนบุรี','วิธีการเดินทางท่องเที่ยว?','การเดินทางท่องเที่ยวในจังหวัดกาญจนบุรี คุณมีความสนใจด้านใดมากที่สุด?','ท่านพักคางพักคืนในการท่องเที่ยวนี้หรือไม่','ในการเดินทางท่องเที่ยวจังหวัดกาญจนบุรีครั้งนี้ คุณมีค่าใช้จ่ายไปทั้งหมดเท่าไร?','คุณชอบกิจกรรมแบบไหนมากที่สุด','คุณชอบกิจกรรมแบบไหนมากที่สุด.1','คุณชอบกิจกรรมแบบไหนมากที่สุด.2','คุณชอบกิจกรรมแบบไหนมากที่สุด.3','คุณชอบกิจกรรมแบบไหนมากที่สุด.4','คุณชอบกิจกรรมแบบไหนมากที่สุด.5']


#create a for loop to iterate through each column in the data
for columns in cat_cols:
    encode(result[columns])

print(result.head())

print('encode_data.info()')
encode_data = encode_data(result)
print(encode_data)



# split data into training and test sets; set random state to 0 for reproducibility 
X_train, X_test, y_train, y_test = train_test_split(encode_data[['คุณมาจากภูมิลําเนาใด', 'รายได้ต่อเดือน', 'อาชีพ', 'อายุ', 'เพศ',
       'โครงสร้างครอบครัว',
       'ความถี่ของการเดินทางท่องเที่ยวของท่านมีลักษณะอยู่ในข้อใด?',
       'จุดมุ่งหมายในการมายังจังหวัดกาญจนบุรี', 'วิธีการเดินทางท่องเที่ยว?',
       'การเดินทางท่องเที่ยวในจังหวัดกาญจนบุรี คุณมีความสนใจด้านใดมากที่สุด?',
       'ท่านพักคางพักคืนในการท่องเที่ยวนี้หรือไม่',
       'ในการเดินทางท่องเที่ยวจังหวัดกาญจนบุรีครั้งนี้ คุณมีค่าใช้จ่ายไปทั้งหมดเท่าไร?',
       'คุณชอบกิจกรรมแบบไหนมากที่สุด', 'คุณชอบกิจกรรมแบบไหนมากที่สุด.1',
       'คุณชอบกิจกรรมแบบไหนมากที่สุด.2', 'คุณชอบกิจกรรมแบบไหนมากที่สุด.3',
       'คุณชอบกิจกรรมแบบไหนมากที่สุด.4', 'คุณชอบกิจกรรมแบบไหนมากที่สุด.5',
       'class']], encode_data['class'], random_state=0)

# see how data has been split
print("X_train shape: {}\ny_train shape: {}".format(X_train.shape, y_train.shape))
print("X_test shape: {}\ny_test shape: {}".format(X_test.shape, y_test.shape))

# initialize the Estimator object
knn = KNeighborsClassifier(n_neighbors=1)

# fit the model to training set in order to predict classes
knn.fit(X_train, y_train)

# what is our score?
print("Test set score: {:.2f}".format(knn.score(X_test, y_test)))

answer = knn.predict(X_test)

print(answer)