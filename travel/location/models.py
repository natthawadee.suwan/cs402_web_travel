from django.db import models
from django.utils import timezone
from taggit.managers import TaggableManager
from django.utils.translation import ugettext_lazy as _



class CategoryMain(models.Model):
    name = models.CharField(_('หมวดหมู่หลัก'), max_length=100)
    # en_name = models.CharField(max_length = 100, help_text = 'example nature')
    # th_name = models.CharField(max_length = 100, help_text = 'ตัวอย่าง ธรรมชาติ')
    # def __str__(self):
    #     return '{} - {}'.format(self.en_name, self.th_name)
 
    # en_name = models.CharField(max_length = 255, help_text = 'example disaster')
    # th_name = models.CharField(max_length = 255, help_text = 'ตัวอย่าง ภัยพิบัติ')
    # en_brief_description = models.TextField(default='', help_text = 'example tu walk and run at thammasat hospital')
    # th_brief_description = models.TextField(default='', help_text = 'ตัวอย่าง TU เดินวิ่งที่โรงพยาบาลธรรมศาสตร์')
    # def __str__(self):
    #     return '{} - {}'.format(self.en_name, self.th_name)

    def __str__(self):
        return self.name

    # cat1 = CategoryMain.objects.get(pk=1)
    # subCat1 = cat1.CategorySub_category_main.all()
    # subFk = CategorySub.objects.filter(category_main)


class CategorySub(models.Model):
    name                = models.CharField(_('หมวดหมู่ย่อย'), max_length=100)
    category_main       = models.ForeignKey(CategoryMain, related_name="CategorySub_category_main", on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name


class Location(models.Model):
    # CHOICES
    ADVENTURE = [
        ('Hard', 'Hard'),
        ('Soft', 'Soft'),
        ('Non-adventure', 'Non-adventure'),
    ]
    class TypeSeason(models.TextChoices):
        WINTER = 'TW', _('Winter Season')
        RAIN = 'TR', _('Rain Season')
        HOT = 'TH', _('Hot Season'),
        Every = 'TA', _('Every Season')

    class SuitablePerson(models.TextChoices):
        CHILD = 'SC', _('Child')
        TEEN = 'ST', _('Teen')
        ELDER = 'SE', _('Elder'),
        Every = 'SA', _('Everyone')

    # DATABASE FIELDS
    # category_main       = models.ForeignKey(CategoryMain, on_delete=models.CASCADE, null=True)
    category_sub        = models.ForeignKey(CategorySub, on_delete=models.CASCADE, null=True)
    name                = models.CharField(_('ชื่อสถานที่'), max_length=100, help_text = 'ตัวอย่าง: ต้นจามจุรียักษ์')
    # name                = models.CharField(_('ชื่อสถานที่'), max_length=100, help_text = 'ตัวอย่าง: ต้นจามจุรียักษ์')
    cost                = models.FloatField(_('ค่าเข้าสถานที่'), null=True, blank=True,help_text = 'ตัวอย่าง: 0')
    lat                 = models.FloatField(_('ละติจูด'), null=True, blank=True, help_text = 'ตัวอย่าง: 0')
    lng                 = models.FloatField(_('ลองติจูด'), null=True, blank=True, help_text = 'ตัวอย่าง: 0')
    time_open           = models.TimeField(_('เวลาเปิดของสถานที่'), null=True, blank=True, default=timezone.now, help_text='ตัวอย่าง 09:00:00 (ชั่วโมง:นาที:วินาที)')
    time_close          = models.TimeField(_('เวลาปิดของสถานที่'), null=True, blank=True, default=timezone.now, help_text='ตัวอย่าง 09:00:00 (ชั่วโมง:นาที:วินาที)')
    shop                = models.BooleanField(_('ร้านค้า'), default=False, help_text = 'กด คือ สถานที่มีร้านอาหาร, ไม่กด คือ สถานที่ไม่มีร้านอาหาร ')
    detail              = models.TextField(_('รายละเอียดสถานที่'), null=True, blank=True)
    tags 				= TaggableManager(_('แท็ก'), blank=True, help_text='เพิ่ม tag แหล่งท่องเที่ยวโดยพิมพ์ tag แล้วกด Enter หรือ ใส , แล่วตามด้วย tag ต่อไป')
    type_adventure      = models.CharField(null=True, blank=True, max_length=50, choices=ADVENTURE)
    type_season         = models.CharField(null=True, blank=True, max_length=2, choices=TypeSeason.choices)
    suitable_person     = models.CharField(null=True, blank=True, max_length=2, choices=SuitablePerson.choices)
   
    # META CLASS
    class Meta:
        verbose_name = 'location'
        verbose_name_plural = 'locations'

    # TO STRING METHOD
    def __str__(self):
        return self.name
    

class Provinces(models.Model):
    name = models.CharField(max_length=100, help_text = 'ตัวอย่าง: ต้นจามจุรียักษ์') 

    def __str__(self):
        return self.name



def upload_location(instance, filename):
	file_path = 'location/{name}/{filename}'.format(
				    name=str(instance.location_gallery.name), filename=filename)
	return file_path


class LocationGallery(models.Model):
    image		 		= models.ImageField(upload_to=upload_location, null=True, blank=True)
    location_gallery    = models.ForeignKey(Location, on_delete=models.CASCADE, related_name="location_image")
