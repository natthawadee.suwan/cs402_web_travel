from django.urls import include, path
from . import views

app_name = 'location'

urlpatterns = [
    path('search/', views.search, name='search'),

    # path('', views.LocationListView.as_view(), name='location_list'),
    path('', views.location_list, name='location_list'),
    path('detail/<int:pk>/', views.LocationDetail, name='location_detail'),
]