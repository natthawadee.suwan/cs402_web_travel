from django import forms

from location.models import Location, LocationGallery
from django.utils.translation import ugettext_lazy as _


class LocationCreateForm(forms.ModelForm):

	class Meta:
		model = Location
		fields = ['name', 'category_sub', 'type_adventure', 'type_season', 'suitable_person', 'cost', 'tags', 'lat', 'lng', 'time_open', 'time_close', 'shop', 'detail']
		widgets = {
            'tags':forms.TextInput(attrs={'data-role': 'tagsinput', 'value': "จุดถ่ายรูป"}),
        }

        # labels = {
        #     'name': _('Writer'),
        # }
        # help_texts = {
        #     'name': _('Some useful help text.'),
        # }
        # error_messages = {
        #     'name': {
        #         'max_length': _("This writer's name is too long."),
        #     },
        # }

class LocationUpdateForm(forms.ModelForm):

	class Meta:
		model = Location
		fields = ['name', 'category_sub', 'type_adventure', 'type_season', 'suitable_person', 'cost', 'tags', 'lat', 'lng', 'time_open', 'time_close', 'shop', 'detail']
		widgets = {
            'tags':forms.TextInput(attrs={'data-role': 'tagsinput', 'value': "จุดถ่ายรูป"}),
        }

	# def save(self, commit=True):
	# 	blog_post = self.instance
	# 	blog_post.title = self.cleaned_data['title']
	# 	blog_post.tags = self.cleaned_data['tags']
	# 	blog_post.body = self.cleaned_data['body']

	# 	if self.cleaned_data['image']:
	# 		blog_post.image = self.cleaned_data['image']

	# 	if commit:
	# 		blog_post.save()
	# 	return blog_post

