from location.models import Location
import django_filters
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_filters import DateRangeFilter, DateFilter, TimeFilter
from django_filters.widgets import RangeWidget
from django import forms


class LocationFilter(django_filters.FilterSet):
    
    LOCATION_NAME = [tuple([x.name,x.name]) for x in Location.objects.all()]
    
    class TypeSeason(models.TextChoices):
        WINTER = 'SW', _('Winter')
        RAIN = 'SR', _('Rain')
        HOT = 'SH', _('Hot')
    
    # start_date = django_filters.NumberFilter(field_name='time_open', lookup_expr='gt')
    # start_date = DateFilter(field_name='time_open',lookup_expr=('lt'),) 

    # first_name = django_filters.CharFilter(lookup_expr='icontains')
    # year_joined = django_filters.NumberFilter(name='date_joined', lookup_expr='year')
    # name = django_filters.ModelMultipleChoiceFilter(queryset=Location.objects.all())
    # name = django_filters.ModelMultipleChoiceFilter(queryset=Location.objects.all(), widget=forms.Select(choices=INTEGER_CHOICES))
    # start_date = TimeFilter(field_name='time_open')
    # start_date = django_filters.DateFromToRangeFilter(widget=RangeWidget(attrs={'type': 'time'}))

    name = django_filters.ChoiceFilter(label="สถานที่ท่องเที่ยว", choices=LOCATION_NAME)
    cost = django_filters.NumberFilter()
    high_season = django_filters.ChoiceFilter(label="High season", choices=TypeSeason)

    class Meta:
        model = Location
        fields = ['name', 'category_sub', 'high_season', 'cost']
        # widgets = {
        #     'cost' : RangeInput(attrs={'max': 100000,
        #     'min':10000,
        #     'step':5000}),

        # }



