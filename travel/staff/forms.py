from django import forms
from hotel.models import Photo

# Using djangos Model forms to create a form using a model.


class PhotoForm(forms.ModelForm):
    class Meta:
        model = Photo
        fields = ['path', ]
