from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, DetailView, ListView, DeleteView, UpdateView
from django.contrib.auth.decorators import login_required

from question.decorators import staff_required
# from trip.models import Season, Landmark, Style
from general.models import Contact, ContactDetail

# @login_required
# @staff_required
# def other_staff(request):
#     season = Season.objects.all()
#     style = Style.objects.all()
#     landmark = Landmark.objects.all()
    
#     data = {
#         'season': season,
#         'style': style,
#         'landmark': landmark,
#     }

#     return render(request, "staff/other/other_staff.html", data)


@login_required
@staff_required
def contact_staff(request):
    contact = Contact.objects.all()
    contact_detail = ContactDetail.objects.all()

    data = {
        'contact': contact,
        'contact_detail': contact_detail,
    }

    return render(request, "staff/other/contact_staff.html", data)
    