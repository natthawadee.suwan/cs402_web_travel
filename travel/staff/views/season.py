from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, DetailView, ListView, DeleteView, UpdateView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required

from question.decorators import staff_required
# from location.models import Season


# @method_decorator([login_required, staff_required], name='dispatch')
# class SeasonCreateView(CreateView):
#     model = Season
#     context_object_name = 'season'
#     template_name = 'staff/season/season_create.html'
#     fields = ['name', ]
#     def get_success_url(self):
#         return reverse_lazy('other_staff')


# @method_decorator([login_required, staff_required], name='dispatch')
# class SeasonUpdateView(UpdateView):
#     model = Season
#     context_object_name = 'season'
#     template_name = 'staff/season/season_update.html'
#     fields = ['name', ]
#     def get_success_url(self):
#         return reverse_lazy('other_staff')


# @method_decorator([login_required, staff_required], name='dispatch')
# class SeasonDeleteView(DeleteView):
#     model = Season
#     context_object_name = 'season'
#     template_name = 'staff/season/season_delete.html'
#     def get_success_url(self):
#         return reverse_lazy('other_staff')


