from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.forms import inlineformset_factory
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.decorators import method_decorator
from django.views.generic import (CreateView, DeleteView, DetailView, ListView,
                                  UpdateView)
from account.models import Account
from question.decorators import staff_required, admin_required
from question.forms import BaseAnswerInlineFormSet, QuestionForm, StaffSignUpForm

from location.models import Location
from trip.models import Trip, Style
from general.models import Contact, ContactDetail
from taggit.models import Tag

class StaffSignUpView(CreateView):
    model = Account
    form_class = StaffSignUpForm
    # template_name = 'staff/signup_staff.html'
    template_name = 'registration/signup_form.html'

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'staff'
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        # return redirect('question_admin:quiz_change_list')
        return redirect('home_admin')


import pandas as pd
import numpy as np
# from sklearn.experimental import enable_iterative_imputer  # noqa
# from sklearn.impute import IterativeImputer
# from sklearn.ensemble import ExtraTreesRegressor
# from sklearn.preprocessing import OrdinalEncoder
# import seaborn as sns

def home_view(request):
	location = Location.objects.all().count()
	style = Style.objects.all().count()
	trip = Trip.objects.all().count()
	common_tags = Trip.tags.most_common().count()


	data = {
		'location': location,
		'tags': common_tags,
		'style': style,
		'trip': trip,
	}
	return render(request, "staff/home.html", data)
