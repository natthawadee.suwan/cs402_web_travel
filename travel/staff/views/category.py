from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, DetailView, ListView, DeleteView, UpdateView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required

from question.decorators import staff_required
from location.models import CategorySub


@method_decorator([login_required, staff_required], name='dispatch')
class CategoryListView(ListView):
    model = CategorySub
    context_object_name = 'categorys'
    template_name = 'staff/category/category_view.html'

    
@method_decorator([login_required, staff_required], name='dispatch')
class CategoryCreateView(CreateView):
    model = CategorySub
    context_object_name = 'category'
    template_name = 'staff/category/category_create.html'
    fields = ['name', 'category_main']

    def get_success_url(self):
        return reverse_lazy('category_admin:category_view')


@method_decorator([login_required, staff_required], name='dispatch')
class CategoryUpdateView(UpdateView):
    model = CategorySub
    context_object_name = 'category'
    template_name = 'staff/category/category_update.html'
    fields = ['name', 'category_main']

    def get_success_url(self):
        return reverse_lazy('category_admin:category_view')


@method_decorator([login_required, staff_required], name='dispatch')
class CategoryDeleteView(DeleteView):
    model = CategorySub
    context_object_name = 'category'
    template_name = 'staff/category/category_delete.html'
    success_url = reverse_lazy('category_admin:category_view')

