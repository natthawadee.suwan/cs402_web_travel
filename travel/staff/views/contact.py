from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, DetailView, ListView, DeleteView, UpdateView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required

from question.decorators import staff_required
from general.models import Contact, ContactDetail


@method_decorator([login_required, staff_required], name='dispatch')
class ContactCreateView(CreateView):
    model = ContactDetail
    context_object_name = 'contact'
    template_name = 'staff/contact/contact_create.html'
    fields = ['name', 'contact', 'phone', 'email', 'address']
    def get_success_url(self):
        return reverse_lazy('contact_staff')


@method_decorator([login_required, staff_required], name='dispatch')
class ContactUpdateView(UpdateView):
    model = ContactDetail
    context_object_name = 'contact'
    template_name = 'staff/contact/contact_update.html'
    fields = ['name', 'contact', 'phone', 'email', 'address']
    def get_success_url(self):
        return reverse_lazy('contact_staff')


@method_decorator([login_required, staff_required], name='dispatch')
class ContactDeleteView(DeleteView):
    model = ContactDetail
    context_object_name = 'contact'
    template_name = 'staff/contact/contact_delete.html'
    def get_success_url(self):
        success_url = reverse_lazy('contact_staff')