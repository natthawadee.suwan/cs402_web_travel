from django.shortcuts import render
from django.urls import reverse_lazy, reverse
from django.views.generic import CreateView, DetailView, ListView, DeleteView, UpdateView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required

from question.decorators import staff_required
from location.models import Location, LocationGallery
from location.forms import LocationCreateForm, LocationUpdateForm
from helper.location import get_location_return_object_image

@login_required
@staff_required
def location_view(request):
    locations = Location.objects.all()
    locations_images = []
    # locations_images = LocationGallery.objects.all()
    # locations_images = locations_images[0]

    location_image = []
    location = []
    location_list = []

    for item in locations:
        location_line = []
        location_line.append(item.name)

    data = {
        'locations': locations,
        'locations_images': locations_images,
    }

    return render(request, "staff/location/location_view.html", data)


@login_required
@staff_required
def location_create_view(request):

	context = {}
	form = LocationCreateForm(request.POST or None, request.FILES or None)
	if form.is_valid():
		form.save()
		form = LocationCreateForm()

	context['form'] = form

	return render(request, "staff/location/location_create.html", context)


@login_required
@staff_required
def location_update_view(request, pk):
	context = {}
	form = LocationUpdateForm(request.POST or None, request.FILES or None)
	if form.is_valid():
		form.save()
		form = LocationUpdateForm()

	context['form'] = form

	return render(request, "staff/location/location_create.html", context)

@login_required
@staff_required
def LocationDetail(request, pk):
    location = Location.objects.filter(pk=pk)
    location = location[0]

    images = get_location_return_object_image(location)

    data = {
        'location': location,
        'images': images,
    }

    return render(request, 'location/location_detail.html', {'data':data})

    
@method_decorator([login_required, staff_required], name='dispatch')
class LocationCreateView(CreateView):
    model = Location
    context_object_name = 'location'
    template_name = 'staff/location/location_create.html'
    fields = ['name', 'category_sub', 'type_adventure', 'type_season', 'suitable_person', 'cost', 'tags', 'lat', 'lng', 'time_open', 'time_close', 'shop', 'detail']

    def get_success_url(self):
        return reverse_lazy('location_admin:location_view')


@method_decorator([login_required, staff_required], name='dispatch')
class LocationUpdateView(UpdateView):
    model = Location
    context_object_name = 'location'
    template_name = 'staff/location/location_update.html'
    fields = ['name', 'category_sub', 'type_adventure', 'type_season', 'suitable_person', 'cost', 'tags', 'lat', 'lng', 'time_open', 'time_close', 'shop', 'detail']

    def get_success_url(self):
        return reverse_lazy('location_admin:location_view')


@method_decorator([login_required, staff_required], name='dispatch')
class LocationDeleteView(DeleteView):
    model = Location
    context_object_name = 'location'
    template_name = 'staff/location/location_delete.html'
    success_url = reverse_lazy('location_admin:location_view')


# @method_decorator([login_required, staff_required], name='dispatch')
class LocationGalleryCreateView(CreateView):
    model = LocationGallery
    fields = ['location_gallery', 'image', ]
    context_object_name = 'location'
    template_name = 'staff/location/location_image.html'
    def get_success_url(self):
        return reverse_lazy('location_admin:location_view')

# @login_required
# @staff_required
# def location_gallery_view(request):
#     location_gallery = LocationGallery.objects.all()
    
#     return render(request, 'staff/location/location_image.html')