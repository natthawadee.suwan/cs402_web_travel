from django.db import models

# Create your models here.
def upload_contact(instance, filename):
	file_path           = 'contact/{name}/{filename}'.format(
				            name=str(instance.name), filename=filename)
	return file_path


class Contact(models.Model):
    name 		        = models.CharField(max_length=50)
    image		 		= models.ImageField(upload_to=upload_contact, null=True, blank=True)
    detail              = models.CharField(max_length=50, blank=True)

    def __str__(self):
        return self.name

class ContactDetail(models.Model):
    name 		        = models.CharField(max_length=50)
    contact             = models.ForeignKey(Contact, on_delete=models.SET_NULL, null=True)
    
    phone               = models.CharField(max_length=50, blank=True)
    email               = models.CharField(max_length=50, blank=True)
    address             = models.TextField(null=True, blank=True)
    # image		 		= models.ImageField(upload_to=upload_contact, null=True, blank=True)

    def __str__(self):
        return self.name



