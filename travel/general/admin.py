from django.contrib import admin
from general.models import Contact, ContactDetail

# Register your models here.

class ContactInline(admin.TabularInline):
    model = ContactDetail


class ContactAdmin(admin.ModelAdmin):
    fieldsets = [('Date Information', {'fields': ['name',('image', 'detail')]}),]
    inlines = [ContactInline]

admin.site.register(Contact, ContactAdmin)

# admin.site.register(Contact)


# class ImagesInline(admin.TabularInline):
#     model = ContactDetailGallery

# class GalleryAdmin(admin.ModelAdmin):
#     inlines = [ImagesInline]

# admin.site.register(ContactDetail, GalleryAdmin) 