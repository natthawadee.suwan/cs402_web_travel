import json
from .models import *
from account.models import Account
from location.models import LocationGallery

def cookie_location(request):

	#Create empty cart for now for non-logged in user
	try:
		cart = json.loads(request.COOKIES['cart'])
	except:
		cart = {}
		print('LOCATION:', cart)

	items = []
	order = {'get_cost_total':0, 'get_time_items':0}
	cartItems = order['get_time_items']

	for i in cart:
		#We use try block to prevent items in cart that may have been removed from causing error
		try:
			cartItems += cart[i]['time']

			location = Location.objects.get(id=i)

			# total = (location.cost * cart[i]['time'])
			total = (location.cost)

			order['get_cost_total'] += total
			order['get_time_items'] += cart[i]['time']


			location_image = LocationGallery.objects.filter(location_gallery = location.id)

			item = {
				'id':location.id,
				'location':{
					'id':location.id,
					'name':location.name, 
					'cost':location.cost, 
				    'image':location_image
					}, 
				'time':cart[i]['time'],
				'get_total':total,
				}
			items.append(item)

		except:
			pass
			
	request.session['countTime'] = order['get_time_items']
	# del request.session['countTime']        

	return {'cartItems':cartItems ,'order':order, 'items':items}

def location_data(request):
	if request.user.is_authenticated:
		customer = request.user
		# customer = request.user.customer
		trip, created = Trip.objects.get_or_create(customer=customer, complete=False)
		items = trip.triplocation_set.all()
		cartItems = trip.get_time_items
	else:
		cookieData = cookie_location(request)
		cartItems = cookieData['cartItems']
		trip = cookieData['order']
		items = cookieData['items']

	return {'cartItems':cartItems ,'order':trip, 'items':items}

	
def guest_trip(request, data):
	name = data['form']['name']
	email = data['form']['email']

	cookieData = cookie_location(request)
	items = cookieData['items']

	print('Email; {}'.format(email))
	customer, created = Account.objects.get_or_create(email=email,)
	customer.name = name
	customer.save()
	print('customer; {}'.format(customer))

	order = Trip.objects.create(
		customer=customer,
		complete=False,
		)

	for item in items:
		location = Location.objects.get(id=item['id'])
		orderItem = TripLocation.objects.create(
			location=location,
			order=order,
			time=item['time'],
		)
	return customer, order

