from django.db import models
from django.db.models.signals import pre_save
from django.utils.text import slugify
from django.conf import settings
from django.db.models.signals import post_delete
from django.dispatch import receiver
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from taggit.managers import TaggableManager
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
# from django.utils.translation import gettext as _ 
from location.models import CategoryMain, CategorySub, Location
from account.models import Account

# CHOICES
class TypeAdventure(models.TextChoices):
    HARD = 'Hard Adventure', _('Hard Adventure')
    SOFT = 'Soft Adventure', _('Soft Adventure')
    NON = 'Non Adventure', _('Non Adventure')

class TypeSeason(models.TextChoices):
    WINTER = 'Winter', _('Winter')
    RAINY = 'Rainy', _('Rainy')
    SUMMER = 'Summer', _('Summer'),
    Every = 'Every Season', _('Every Seasons')

class SuitablePerson(models.TextChoices):
    CHILD = 'Child', _('Child')
    TEEN = 'Teen', _('Teen')
    ADULT = 'Adult', _('Adult'),
    ELDER = 'Elder', _('Elder'),
    Every = 'Everyone', _('Everyone')

class TypeGenerate(models.TextChoices):
    USER = 'User', _('User'),
    SYSTEM = 'System', _('System'),


class Style(models.Model):
    th_name = models.CharField(max_length=100)
    en_name = models.CharField(max_length=100, null=True, blank=True)
    
    def __str__(self):
        return self.th_name


class Trip(models.Model):

    customer        = models.ForeignKey(Account, on_delete=models.SET_NULL, null=True, blank=True)
    date_ordered    = models.DateTimeField(auto_now_add=True)
    complete        = models.BooleanField(default=False)
    # transaction_id  = models.CharField(max_length=100, null=True, unique=True)

    name 				    = models.CharField(null=True, max_length=50, unique=True, help_text='ตัวอย่าง: ทริปธรรมชาติและการถ่ายรูป')
    budget                  = models.IntegerField(default=0, null=True, blank=True)
    time_start              = models.TimeField(default=timezone.now)
    time_hotel              = models.TimeField(default=timezone.now)
    token                   = models.CharField(max_length=50, blank=True)
    location_start          = models.CharField(max_length=50, blank=True)
    person_number           = models.IntegerField(default=0, null=True, blank=True)
    style                   = models.ForeignKey(Style, on_delete=models.SET_NULL, null=True)
    category_sub            = models.ManyToManyField(CategorySub, null=True)
    tags 					= TaggableManager(help_text='เพิ่ม tag แหล่งท่องเที่ยวโดยพิมพ์ tag แล้วกด Enter')
    type_adventure          = models.CharField(null=True, blank=True, max_length=30, choices=TypeAdventure.choices)
    type_season             = models.CharField(null=True, blank=True, max_length=30, choices=TypeSeason.choices)
    suitable_person         = models.CharField(null=True, blank=True, max_length=30, choices=SuitablePerson.choices)
    trip_description_json   = models.TextField(null=True, blank=True)
    trip_description_text   = models.TextField(null=True, blank=True)


    def __str__(self):
        return str(self.name)
    
    @property
    def get_cost_total(self):
        triplocations = self.triplocation_set.all()
        total = sum([item.get_cost for item in triplocations])
        return total 
        
    @property
    def get_time_items(self):
        triplocations = self.triplocation_set.all()
        total = sum([item.time for item in triplocations])
        return total

    class Meta:
        verbose_name = "Trip"
        verbose_name_plural = "Trips"



class TripLocation(models.Model):
	location = models.ForeignKey(Location, on_delete=models.SET_NULL, null=True)
	order = models.ForeignKey(Trip, on_delete=models.SET_NULL, null=True)
	time = models.IntegerField(default=0, null=True, blank=True)
	date_added = models.DateTimeField(auto_now_add=True)

	@property
	def get_cost(self):
		return self.location.cost

