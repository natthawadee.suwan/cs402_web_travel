from django.contrib import admin
from trip.models import Style, Trip, TripLocation

# Register your models here.
admin.site.register(Style)


# class TripAdmin(admin.ModelAdmin):
#     # fields = ['id', 'name', 'budget', 'time', ('season', 'location_start'), 'tags', ('child','teens', 'elderly'), 'category', 'location', 'landmark', 'style']
#     readonly_fields = ['id','token', 'name', 'tags', 'trip_description_text', 'trip_description_json', ]
#     list_display = ['name',]
#     # filter_horizontal = ('category_sub')

# admin.site.register(Trip, TripAdmin)


# Trip
class TripLocationInline(admin.TabularInline):
    model = TripLocation

class TripLocationInlineAdmin(admin.ModelAdmin):
    readonly_fields = [ 'name', 'customer', 'complete', 'category_sub', 'token', 'trip_description_text', 'trip_description_json', ]
    inlines = [TripLocationInline]
    # filter_horizontal = ('category_sub')


admin.site.register(Trip, TripLocationInlineAdmin) 
