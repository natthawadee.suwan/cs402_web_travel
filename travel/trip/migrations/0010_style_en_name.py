# Generated by Django 3.0.5 on 2020-05-30 11:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trip', '0009_auto_20200530_1755'),
    ]

    operations = [
        migrations.AddField(
            model_name='style',
            name='en_name',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
