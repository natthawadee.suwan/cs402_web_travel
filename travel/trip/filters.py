from trip.models import Trip, Style
import django_filters, json
from django_filters import DateRangeFilter, DateFilter, TimeFilter
from django_filters.widgets import RangeWidget
from django import forms
from helper.location import load_location_start_end

class TripFilter(django_filters.FilterSet):
    location_start_end = load_location_start_end()

    CHOICES_LOCATIONS = [tuple([x,x]) for x in location_start_end]
    # CHOICES_STYLE = [tuple([x.pk,x.name]) for x in Style.objects.all()]
    # CHOICES_SEASON = [tuple([x.pk,x.name]) for x in Season.objects.all()]
    # CHOICES_LANDMARK = [tuple([x.pk,x.name]) for x in Landmark.objects.all()]
    # CHOICES_GENERATE = [ ('User', 'User'),('System', 'System') ]
    PERSON_NUMBER = [ ('0', '0'), ('1', '1'),('2', '2') ]

    # style = django_filters.ChoiceFilter(label="รูปแบบการท่องเที่ยว", choices=CHOICES_STYLE)
    # season = django_filters.ChoiceFilter(label="ฤดูท่องเที่ยว", choices=CHOICES_SEASON)
    # landmark = django_filters.ChoiceFilter(label="จุดเด่น", choices=CHOICES_LANDMARK)
    location_start = django_filters.ChoiceFilter(choices=CHOICES_LOCATIONS)
    budget         = django_filters.NumberFilter()
    person_number  = django_filters.NumberFilter()
    # type_generate = django_filters.ChoiceFilter(label="รูปแบบการสร้าง", choices=CHOICES_GENERATE)
    # person_number = django_filters.ChoiceFilter(label="Person Number", choices=PERSON_NUMBER)

    class Meta:
        model = Trip
        fields = ['name', 'budget', 'location_start', 'person_number']




