from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.db.models import Count
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import CreateView, ListView, UpdateView
from account.models import Account
from question.decorators import active_required
from question.forms import ActiveInterestsForm, ActiveSignUpForm, TakeQuizForm
from question.models import Quiz, Active, TakenQuiz


class ActiveSignUpView(CreateView):
    model = Account
    form_class = ActiveSignUpForm
    template_name = 'registration/signup_form.html'

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'active'
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('active:quiz_list')


@method_decorator([login_required, active_required], name='dispatch')
class ActiveInterestsView(UpdateView):
    model = Active
    form_class = ActiveInterestsForm
    template_name = 'question/active/interests_form.html'
    success_url = reverse_lazy('active:quiz_list')

    def get_object(self):
        return self.request.user.active

    def form_valid(self, form):
        messages.success(self.request, 'Interests updated with success!')
        return super().form_valid(form)


@method_decorator([login_required, active_required], name='dispatch')
class QuizListView(ListView):
    model = Quiz
    ordering = ('name', )
    context_object_name = 'quizzes'
    template_name = 'question/active/quiz_list.html'

    def get_queryset(self):
        active = self.request.user.active
        print(active)
        active_interests = active.interests.values_list('pk', flat=True)
        print(active_interests)

        taken_quizzes = active.quizzes.values_list('pk', flat=True)
        print (taken_quizzes)
        queryset = Quiz.objects.filter(subject__in=active_interests) \
            .exclude(pk__in=taken_quizzes) \
            .annotate(questions_count=Count('questions')) \
            .filter(questions_count__gt=0)
        return queryset


@method_decorator([login_required, active_required], name='dispatch')
class TakenQuizListView(ListView):
    model = TakenQuiz
    context_object_name = 'taken_quizzes'
    template_name = 'question/active/taken_quiz_list.html'

    def get_queryset(self):
        queryset = self.request.user.active.taken_quizzes \
            .select_related('quiz', 'quiz__subject') \
            .order_by('quiz__name')
        return queryset


@login_required
@active_required
def take_quiz(request, pk):
    quiz = get_object_or_404(Quiz, pk=pk)
    active = request.user.active

    if active.quizzes.filter(pk=pk).exists():
        return render(request, 'active/taken_quiz.html')

    total_questions = quiz.questions.count()
    unanswered_questions = active.get_unanswered_questions(quiz)
    total_unanswered_questions = unanswered_questions.count()
    progress = 100 - round(((total_unanswered_questions - 1) / total_questions) * 100)
    question = unanswered_questions.first()
    print('total_questions: {}'.format(total_questions))
    print('unanswered_questions: {}'.format(unanswered_questions))
    print('total_unanswered_questions: {}'.format(total_unanswered_questions))
    print('progress: {}'.format(progress))
    print('question: {}'.format(question))


    if request.method == 'POST':
        form = TakeQuizForm(question=question, data=request.POST)

        # print("form: {}".format(form))

        if form.is_valid():
            with transaction.atomic():
                active_answer = form.save(commit=False)
                active_answer.active = active
                active_answer.save()

                # print("======================================================")
                # print("quiz: {}".format(quiz))
                # print("subject: {}".format(quiz.subject))
                # print('question: {}'.format(question))
                # print("active: {}".format(active_answer.active))
                # print("answer: {}".format(active_answer.answer))
                # print("======================================================")

                if active.get_unanswered_questions(quiz).exists():
                    return redirect('active:take_quiz', pk)
                else:

                    correct_answers = active.quiz_answers.filter(answer__question__quiz=quiz, answer__is_correct=True).count()                    
                    score = round((correct_answers / total_questions) * 100.0, 2)
                    TakenQuiz.objects.create(active=active, quiz=quiz, score=score)
                    if score < 50.0:
                        messages.warning(request, 'Better luck next time! Your score for the quiz %s was %s.' % (quiz.name, score))
                    else:
                        messages.success(request, 'Congratulations! You completed the quiz %s with success! You scored %s points.' % (quiz.name, score))
                    return redirect('active:quiz_list')
    else:
        form = TakeQuizForm(question=question)

    return render(request, 'question/active/take_quiz_form.html', {
        'quiz': quiz,
        'question': question,
        'form': form,
        'progress': progress
    })
