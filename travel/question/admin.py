from django.contrib import admin
from question.models import Subject, Active, Question, TakenQuiz, Quiz, Answer, ActiveAnswer

# Register your models here.

admin.site.register(Subject)
admin.site.register(Active)
admin.site.register(Question)
admin.site.register(TakenQuiz)
admin.site.register(Quiz)
admin.site.register(Answer)
admin.site.register(ActiveAnswer)

